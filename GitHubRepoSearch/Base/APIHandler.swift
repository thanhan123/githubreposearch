//
//  APIHandler.swift
//  CarChoosing
//
//  Created by Dinh Thanh An on 2019/10/23.
//  Copyright © 2019 Dinh Thanh An. All rights reserved.
//

import Foundation

protocol APIHandler {
  func fetch(baseURL: String, params: [String: String], completion: @escaping (Result<Data, CustomError>) -> Void)
}

final class APIHandlerImpl: APIHandler {
  private lazy var session: URLSession = {
    let session = URLSession.shared
    return session
  }()

  func fetch(baseURL: String, params: [String: String], completion: @escaping (Result<Data, CustomError>) -> Void) {
    guard var components = URLComponents(string: baseURL) else {
      return
    }

    components.queryItems = params.map { (arg) -> URLQueryItem in
      let (key, value) = arg
      return URLQueryItem(name: key, value: value)
    }

    guard let url = components.url
    else { preconditionFailure("Can't create url for baseURL: \(baseURL)") }

    let dataTask = session.dataTask(with: URLRequest(url: url)) { data, response, error in
      if let data = data {
        completion(.success(data))
        return
      }
      completion(.failure(.noObject))
    }
    dataTask.resume()
  }
}
