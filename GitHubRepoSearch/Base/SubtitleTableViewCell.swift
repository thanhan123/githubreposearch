//
//  SubtitleTableViewCell.swift
//  MoneyExchange
//
//  Created by Dinh Thanh An on 11/17/19.
//  Copyright © 2019 Dinh Thanh An. All rights reserved.
//

import UIKit

final class SubtitleTableViewCell: UITableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
