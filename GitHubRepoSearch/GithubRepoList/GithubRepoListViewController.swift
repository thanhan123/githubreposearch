//
//  GithubRepoListViewController.swift
//  MoneyExchange
//
//  Created by dinh thanh an on 2022/01/22.
//  Copyright © 2022 Dinh Thanh An. All rights reserved.
//

import UIKit

final class GithubRepoListViewController: BaseViewController<GithubRepoListView> {
  private var viewModel: GithubRepoListViewModelProtocol
  private lazy var customDataSource = TableViewDataSource<GithubRepo>(
    models: [],
    reuseIdentifier: SubtitleTableViewCell.name,
    cellConfigurator: { (repo, cell, indexPath) in
      cell.textLabel?.text = repo.name
      cell.detailTextLabel?.text = repo.description
    })

  init(viewModel: GithubRepoListViewModelProtocol) {
    self.viewModel = viewModel
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func loadView() {
    view = GithubRepoListView(
      tableViewDataSource: customDataSource,
      tableViewDelegate: self,
      delegate: self
    )
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = UIColor.white

    viewModel.githubReposHandler = { [weak self] repos in
      self?.customDataSource.models = repos
      self?.currentView.tableView.reloadData()
    }

    viewModel.loadingHandler = { [weak self] value in
      if value {
        self?.currentView.indicator.startAnimating()
      } else {
        self?.currentView.indicator.stopAnimating()
      }
    }
  }
}

extension GithubRepoListViewController: UITableViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    view.endEditing(true)
  }
}

extension GithubRepoListViewController: GithubRepoListViewDelegate {
  func textFieldEditing(text: String) {
    viewModel.search(name: text)
  }
}
