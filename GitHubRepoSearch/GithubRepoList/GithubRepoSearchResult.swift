//
//  GithubRepoSearchResult.swift
//  MoneyExchange
//
//  Created by dinh thanh an on 2022/01/22.
//  Copyright © 2022 Dinh Thanh An. All rights reserved.
//

import Foundation

struct GithubRepoSearchResult: Decodable {
  let items: [GithubRepo]
}
