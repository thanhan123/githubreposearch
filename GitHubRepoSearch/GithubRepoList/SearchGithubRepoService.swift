//
//  SearchGithubRepoService.swift
//  MoneyExchange
//
//  Created by dinh thanh an on 2022/01/22.
//  Copyright © 2022 Dinh Thanh An. All rights reserved.
//

import Foundation

private let baseURL = "https://api.github.com"

protocol SearchGithubRepoService {
  func search(name: String, completion: @escaping ([GithubRepo]) -> Void)
}

final class SearchGithubRepoServiceImpl: SearchGithubRepoService {
  private let apiPath = "/search/repositories"
  private let apiHandler: APIHandler

  init(apiHandler: APIHandler = APIHandlerImpl()) {
    self.apiHandler = apiHandler
  }

  func search(name: String, completion: @escaping ([GithubRepo]) -> Void) {
    apiHandler.fetch(baseURL: "\(baseURL)\(apiPath)", params: ["q":name]) { result in
      switch result {
      case .success(let data):
        do {
          let repos = try JSONDecoder().decode(GithubRepoSearchResult.self, from: data).items
          completion(repos)
        } catch {
          completion([])
        }
      case .failure:
        completion([])
      }
    }
  }
}
