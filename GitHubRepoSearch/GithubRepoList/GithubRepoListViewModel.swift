//
//  GithubRepoListViewModel.swift
//  MoneyExchange
//
//  Created by dinh thanh an on 2022/01/22.
//  Copyright © 2022 Dinh Thanh An. All rights reserved.
//

import Foundation

protocol GithubRepoListViewModelOutput {
  var githubReposHandler: (([GithubRepo]) -> Void)? { get set }
  var loadingHandler: ((Bool) -> Void)? { get set }
}

protocol GithubRepoListViewModelInput {
  func search(name: String)
}

typealias GithubRepoListViewModelProtocol = GithubRepoListViewModelOutput & GithubRepoListViewModelInput

final class GithubRepoListViewModel: GithubRepoListViewModelProtocol {
  var githubReposHandler: (([GithubRepo]) -> Void)?
  var loadingHandler: ((Bool) -> Void)?
  private var workItem: DispatchWorkItem?

  private let searchGithubRepoService: SearchGithubRepoService

  init(searchGithubRepoService: SearchGithubRepoService = SearchGithubRepoServiceImpl()) {
    self.searchGithubRepoService = searchGithubRepoService
  }

  func search(name: String) {
    workItem?.cancel()
    if name == "" {
      githubReposHandler?([])
      return
    }
    workItem = DispatchWorkItem(block: { [weak self] in
      DispatchQueue.main.async {
        self?.loadingHandler?(true)
      }
      self?.searchGithubRepoService.search(name: name, completion: { repos in
        DispatchQueue.main.async {
          self?.loadingHandler?(false)
          self?.githubReposHandler?(repos)
        }
      })
    })
    DispatchQueue.global(qos: .userInitiated).asyncAfter(deadline: .now() + 0.5, execute: workItem!)
  }
}
