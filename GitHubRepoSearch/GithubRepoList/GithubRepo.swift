//
//  GithubRepo.swift
//  MoneyExchange
//
//  Created by dinh thanh an on 2022/01/22.
//  Copyright © 2022 Dinh Thanh An. All rights reserved.
//

import Foundation

struct GithubRepo: Decodable {
  let id: Int64
  let name: String
  let description: String?
  var url: String {
    return html_url
  }
  private let html_url: String
}
