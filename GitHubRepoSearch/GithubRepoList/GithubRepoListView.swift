//
//  GithubRepoListView.swift
//  MoneyExchange
//
//  Created by dinh thanh an on 2022/01/22.
//  Copyright © 2022 Dinh Thanh An. All rights reserved.
//

import UIKit

protocol GithubRepoListViewDelegate: AnyObject {
  func textFieldEditing(text: String)
}

final class GithubRepoListView: BaseView {
  private lazy var searchTextField: UITextField = {
    let textField = UITextField()
    textField.translatesAutoresizingMaskIntoConstraints = false
    textField.delegate = self
    textField.borderStyle = .roundedRect
    textField.returnKeyType = .done
    textField.autocorrectionType = .no
    textField.borderStyle = .line
    textField.placeholder = "Search Repos..."
    return textField
  }()

  private weak var delegate: GithubRepoListViewDelegate?
  var indicator: UIActivityIndicatorView = {
    let indicator = UIActivityIndicatorView()
    indicator.color = .black
    indicator.style = .medium
    indicator.translatesAutoresizingMaskIntoConstraints = false
    return indicator
  }()

  let tableView: UITableView = {
    let tableView = UITableView()
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.register(SubtitleTableViewCell.self, forCellReuseIdentifier: SubtitleTableViewCell.name)
    tableView.keyboardDismissMode = .interactive
    return tableView
  }()

  init(tableViewDataSource: UITableViewDataSource,
       tableViewDelegate: UITableViewDelegate,
       delegate: GithubRepoListViewDelegate?) {
    tableView.dataSource = tableViewDataSource
    tableView.delegate = tableViewDelegate
    self.delegate = delegate
    super.init(frame: .zero)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func setupView() {
    super.setupView()

    addSubview(searchTextField)
    addSubview(tableView)
    addSubview(indicator)

    NSLayoutConstraint.activate(
      [
        searchTextField.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 8.0),
        searchTextField.leftAnchor.constraint(equalTo: safeAreaLayoutGuide.leftAnchor, constant: 8.0),
        searchTextField.rightAnchor.constraint(equalTo: safeAreaLayoutGuide.rightAnchor, constant: -8.0),

        tableView.topAnchor.constraint(equalTo: searchTextField.bottomAnchor, constant: 8.0),
        tableView.leftAnchor.constraint(equalTo: safeAreaLayoutGuide.leftAnchor, constant: 8.0),
        tableView.rightAnchor.constraint(equalTo: safeAreaLayoutGuide.rightAnchor, constant: -8.0),
        tableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -8.0),

        indicator.centerXAnchor.constraint(equalTo: tableView.centerXAnchor),
        indicator.centerYAnchor.constraint(equalTo: tableView.centerYAnchor)
      ]
    )
  }
}

extension GithubRepoListView: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }

  func textFieldDidEndEditing(_ textField: UITextField) {
    textField.resignFirstResponder()
  }

  func textField(_ textField: UITextField,
                 shouldChangeCharactersIn range: NSRange,
                 replacementString string: String) -> Bool {
    if let text = textField.text,
       let textRange = Range(range, in: text) {
      let updatedText = text.replacingCharacters(in: textRange,
                                                 with: string)
      delegate?.textFieldEditing(text: updatedText)
    }
    return true
  }
}
